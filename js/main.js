
/*
 Quand on clique sur le button (.onclick),,il demande d'entrer le nom de utilisature 
 On a deux option .... 
-- soit il insert the nom Soit Pas .. sinon 
-- Effect Duration
-- Select block container
-- Create Array from game blocks
-- Create Range of keys
-- Spread operator
-- ADD order Css property to game blocks
-- Sfuffle Function:
            -- [1] save current element in stash
            -- [2] Current Elemnt = Random element
            -- [3] Random Element = Get Element from Stash
-- => add class is-flipped 
-- => filiter 
-- => is ther is two element have class(is-flipped) 
-- => add new class(no-click) by stopclickin function.
-- => after duration => remove no-clicking class
*/



document.querySelector(".control-buttons span").onclick = function(){

    let yourName = prompt("What's your name");
    console.log(yourName);
    if(yourName == null || yourName == ""){

        document.querySelector(".name span").innerHTML = 'Unknown';
    }
    else{
        document.querySelector(".name span").innerHTML = yourName;
    }
    // remove le button après
    document.querySelector(".control-buttons").remove(); 
    
    document.getElementById('success').play();
};

// Effect Duration
let duration = 1000;


// select block container
let blockContainer = document.querySelector(".memory-game-container");


// Create Array from game blocks
let blocks = Array.from(blockContainer.children);


// Create Range of keys

// let orderRange = [...Array(blocks.length).keys()];    /*Spread operator*/ 

let orderRange = Array.from(Array(blocks.length).keys());

// console.log(orderRange);
shuffle(orderRange);
// console.log(orderRange);





blocks.forEach((block, index)=>{

    // ADD order CSS property to game blocks
    block.style.order = orderRange[index];

    // Add click event
    block.addEventListener('click', function(){

       // trigger the flipBlock function 
       flipBlock(block);

    });
});




// flip block function

function flipBlock(selectBlock){
  
  // flip block function
  selectBlock.classList.add('is-flipped');

  // collect all flipped cards
  let allflippedBlocks = blocks.filter(flippedBlock => flippedBlock.classList.contains('is-flipped'));

  // if there two selected blocks
  if(allflippedBlocks.length === 2){
 
  // stop clicking function
    stopclicking();
  // check matched block
    checkMatched(allflippedBlocks[0],allflippedBlocks[1]);

  }


}




/* functions */

// Sfuffle Function

function shuffle(Array){

  // setting var
  let current = Array.length,
      temp,
      random;
      
    while(current > 0){
        // Get random number
        random = Math.floor(Math.random() * current);

        // Decrease Lenfth by on
        current--;
        // [1] save current element in stash
        temp = Array[current];
        // [2] Current Elemnt = Random element
        Array[current] = Array[random];
        // Random Element = Get Element from Stash
        Array[random] = temp;
    }

      return Array;
}


// stop clicking function
function stopclicking(){

    // add class no clicking on main container
    blockContainer.classList.add('no-clicking');

    // remove no-clicking class after the duration

    setTimeout(()=>{
        blockContainer.classList.remove('no-clicking');
    }, duration);
}



// check matched block
function checkMatched(firstBlock, secondBlock){

    let triesElement = document.querySelector('.tries span');
    let welltriesElement = document.querySelector('.welltries span');

    if (firstBlock.dataset.technologie === secondBlock.dataset.technologie){
       
        firstBlock.classList.remove('is-flipped');
        secondBlock.classList.remove('is-flipped');
        
        // Remove class => add class

        firstBlock.classList.add('has-match');
        secondBlock.classList.add('has-match');

        welltriesElement.innerHTML = parseInt(welltriesElement.innerHTML) +1;   //parseInt: Converts a string to an integer.
        
        document.getElementById('success').play();
        // document.querySelector(".control-buttons").(); 
    }
    else{

        triesElement.innerHTML = parseInt(triesElement.innerHTML) +1;


        setTimeout(() => {
            firstBlock.classList.remove('is-flipped');
            secondBlock.classList.remove('is-flipped');

            document.getElementById('fail').play();

        },duration);
    }
}